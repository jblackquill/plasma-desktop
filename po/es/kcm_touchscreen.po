# Spanish translations for kcm_touchscreen.po package.
# Copyright (C) 2022 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Automatically generated, 2022.
# Eloy Cuadra <ecuadra@eloihr.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kcm_touchscreen\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-09 02:14+0000\n"
"PO-Revision-Date: 2022-11-24 02:10+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.11.80\n"

#: kcmtouchscreen.cpp:44
#, kde-format
msgid "Automatic"
msgstr "Automática"

#: kcmtouchscreen.cpp:50
#, kde-format
msgctxt "model - (x,y widthxheight)"
msgid "%1 - (%2,%3 %4×%5)"
msgstr "%1 - (%2,%3 %4×%5)"

#: ui/main.qml:28
#, kde-format
msgid "No touchscreens found"
msgstr "No se han encontrado pantallas táctiles"

#: ui/main.qml:44
#, kde-format
msgctxt "@label:listbox The device we are configuring"
msgid "Device:"
msgstr "Dispositivo:"

#: ui/main.qml:57
#, kde-format
msgid "Enabled:"
msgstr "Activado:"

#: ui/main.qml:65
#, kde-format
msgid "Target display:"
msgstr "Pantalla de destino:"
