# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Vít Pelčák <vit@pelcak.org>, 2011, 2015, 2016, 2017, 2019, 2020.
# Tomáš Chvátal <tomas.chvatal@gmail.com>, 2012.
# Vit Pelcak <vit@pelcak.org>, 2021, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: kcmcomponentchooser\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-22 02:07+0000\n"
"PO-Revision-Date: 2023-02-03 16:53+0100\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 22.12.1\n"

#: applicationmodel.cpp:67
#, kde-format
msgid "Other…"
msgstr "Jiné…"

#. i18n: ectx: label, entry (browserApplication), group (General)
#: browser_settings.kcfg:9
#, kde-format
msgid "Default web browser"
msgstr "Výchozí webový prohlížeč"

#: components/componentchooserarchivemanager.cpp:15
#, kde-format
msgid "Select default archive manager"
msgstr "Vyberte výchozí správce archivů"

#: components/componentchooserbrowser.cpp:15
#, kde-format
msgid "Select default browser"
msgstr "Vyberte výchozí prohlížeč"

#: components/componentchooseremail.cpp:20
#, kde-format
msgid "Select default e-mail client"
msgstr "Vyberte výchozí poštovní klient"

#: components/componentchooserfilemanager.cpp:15
#, kde-format
msgid "Select default file manager"
msgstr "Vyberte výchozí správce souborů"

#: components/componentchoosergeo.cpp:13
#, kde-format
msgid "Select default map"
msgstr "Vyberte výchozí mapu"

#: components/componentchooserimageviewer.cpp:15
#, kde-format
msgid "Select default image viewer"
msgstr "Vyberte prosím výchozí prohlížeč obrázků"

#: components/componentchoosermusicplayer.cpp:15
#, kde-format
msgid "Select default music player"
msgstr "Vyberte výchozí přehrávač hudby"

#: components/componentchooserpdfviewer.cpp:11
#, kde-format
msgid "Select default PDF viewer"
msgstr "Vyberte výchozí prohlížeč PDF"

#: components/componentchoosertel.cpp:17
#, kde-format
msgid "Select default dialer application"
msgstr "Vyberte výchozí aplikaci pro číselník telefonu"

#: components/componentchooserterminal.cpp:25
#, kde-format
msgid "Select default terminal emulator"
msgstr "Vyberte výchozí emulátor terminálu"

#: components/componentchoosertexteditor.cpp:15
#, kde-format
msgid "Select default text editor"
msgstr "Vyberte prosím výchozí editor textu"

#: components/componentchooservideoplayer.cpp:15
#, kde-format
msgid "Select default video player"
msgstr "Vyberte výchozí přehrávač videa"

#: ui/ComponentOverlay.qml:22
#, kde-format
msgid "Details"
msgstr "Podrobnosti"

#: ui/ComponentOverlay.qml:28
#, kde-format
msgid ""
"This application does not advertise support for the following file types:"
msgstr "Tato aplikace nehlásí podporu následujících typů souborů:"

#: ui/ComponentOverlay.qml:42
#, kde-format
msgctxt "@action:button"
msgid "Force Open Anyway"
msgstr "Vynutit otevření i tak"

#: ui/ComponentOverlay.qml:50
#, kde-format
msgid ""
"The following file types are still associated with a different application:"
msgstr "Následující typy souborů jsou stále přiřazené jiné aplikaci:"

#: ui/ComponentOverlay.qml:59
#, kde-format
msgctxt "@label %1 is a MIME type and %2 is an application name"
msgid "%1 associated with %2"
msgstr "%1 přiřazená k %2"

#: ui/ComponentOverlay.qml:65
#, kde-format
msgctxt "@action:button %1 is an application name"
msgid "Re-assign-all to %1"
msgstr "Přiřadit vše k %1"

#: ui/ComponentOverlay.qml:73
#, kde-format
msgid "Change file type association manually"
msgstr "Ručně změnit asociaci typu souboru"

#: ui/main.qml:17
#, kde-format
msgid ""
"’%1’ seems to not support the following mimetypes associated with this kind "
"of application: %2"
msgstr ""
"’%1’ nejspíše nepodporuje následující typy MIME přiřazené tomuto druhu "
"aplikace: %2"

#: ui/main.qml:45
#, kde-format
msgctxt "Internet related application’s category’s name"
msgid "Internet"
msgstr "Internet"

#: ui/main.qml:49
#, kde-format
msgid "Web browser:"
msgstr "Webový prohlížeč:"

#: ui/main.qml:71
#, kde-format
msgid "Email client:"
msgstr "Poštovní klient:"

#: ui/main.qml:93
#, kde-format
msgctxt "Default phone app"
msgid "Dialer:"
msgstr "Číselník telefonu:"

#: ui/main.qml:115
#, kde-format
msgctxt "Multimedia related application’s category’s name"
msgid "Multimedia"
msgstr "Multimédia"

#: ui/main.qml:120
#, kde-format
msgid "Image viewer:"
msgstr "Prohlížeč obrázků:"

#: ui/main.qml:144
#, kde-format
msgid "Music player:"
msgstr "Přehrávač hudby:"

#: ui/main.qml:167
#, kde-format
msgid "Video player:"
msgstr "Video přehrávač:"

#: ui/main.qml:189
#, kde-format
msgctxt "Documents related application’s category’s name"
msgid "Documents"
msgstr "Dokumenty"

#: ui/main.qml:194
#, kde-format
msgid "Text editor:"
msgstr "Textový editor:"

#: ui/main.qml:216
#, kde-format
msgid "PDF viewer:"
msgstr "Prohlížeč PDF:"

#: ui/main.qml:238
#, kde-format
msgctxt "Utilities related application’s category’s name"
msgid "Utilities"
msgstr "Nástroje"

#: ui/main.qml:243
#, kde-format
msgid "File manager:"
msgstr "Správce souborů:"

#: ui/main.qml:265
#, kde-format
msgid "Terminal emulator:"
msgstr "Emulátor terminálu:"

#: ui/main.qml:278
#, kde-format
msgid "Archive manager:"
msgstr "Správce archivů:"

#: ui/main.qml:300
#, kde-format
msgctxt "Map related application’s category’s name"
msgid "Map:"
msgstr "Mapa:"
