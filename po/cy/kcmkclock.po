# translation of kcmkclock.po to Cymraeg
# Translation of kcmkclock.po to Cymraeg
# Copyright (C) 2003, 2004 Free Software Foundation, Inc.
# KGyfieithu <kyfieithu@dotmon.com>, 2003.
# KD at KGyfieithu <kyfieithu@dotmon.com>, 2004.
#
msgid ""
msgstr ""
"Project-Id-Version: kcmkclock\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-04 02:17+0000\n"
"PO-Revision-Date: 2004-07-03 07:15+0100\n"
"Last-Translator: KD at KGyfieithu <kyfieithu@dotmon.com>\n"
"Language-Team: Cymraeg <cy@li.org>\n"
"Language: cy\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "KGyfieithu"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kyfieithu@dotmon.com"

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: dateandtime.ui:22
#, kde-format
msgid "Date and Time"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, setDateTimeAuto)
#: dateandtime.ui:30
#, kde-format
msgid "Set date and time &automatically"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, timeServerLabel)
#: dateandtime.ui:53
#, kde-format
msgid "&Time server:"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (KDatePicker, cal)
#: dateandtime.ui:86
#, kde-format
msgid "Here you can change the system date's day of the month, month and year."
msgstr ""
"Gallwch newid yma diwrnod y mis, mis a blwyddyn sy'n cael eu dangos am "
"ddyddiad y cysawd."

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: dateandtime.ui:122
#, fuzzy, kde-format
#| msgid "Timezone Error"
msgid "Time Zone"
msgstr "Gwall Cylchfa Amser"

#. i18n: ectx: property (text), widget (QLabel, label)
#: dateandtime.ui:128
#, fuzzy, kde-format
msgid "To change the local time zone, select your area from the list below."
msgstr "I newid y gylchfa amser, dewiswch eich ardal o'r rhestr isod:"

#. i18n: ectx: property (text), widget (QLabel, m_local)
#: dateandtime.ui:151
#, fuzzy, kde-format
msgid "Current local time zone:"
msgstr "Cylchfa amser gyfredol:"

#. i18n: ectx: property (placeholderText), widget (KTreeWidgetSearchLine, tzonesearch)
#: dateandtime.ui:161
#, kde-format
msgid "Search…"
msgstr ""

#: dtime.cpp:61
#, kde-format
msgid ""
"No NTP utility has been found. Install 'ntpdate' or 'rdate' command to "
"enable automatic updating of date and time."
msgstr ""

#: dtime.cpp:91
#, kde-format
msgid ""
"Here you can change the system time. Click into the hours, minutes or "
"seconds field to change the relevant value, either using the up and down "
"buttons to the right or by entering a new value."
msgstr ""
"Gallwch newid yma amser y cysawd.  Cliciwch i mewn i'r meysydd oriau, "
"munudau neu eiliadau i newid y gwerth perthnasol, unai gan ddefnyddio y "
"botymau i fyny ac i lawr i'r dde, neu gan fewnosod gwerth newydd."

#: dtime.cpp:113
#, fuzzy, kde-format
msgctxt "%1 is name of time zone"
msgid "Current local time zone: %1"
msgstr "Cylchfa amser gyfredol:"

#: dtime.cpp:116
#, fuzzy, kde-format
msgctxt "%1 is name of time zone, %2 is its abbreviation"
msgid "Current local time zone: %1 (%2)"
msgstr "Cylchfa amser gyfredol:"

#: dtime.cpp:203
#, kde-format
msgid ""
"Public Time Server (pool.ntp.org),        asia.pool.ntp.org,        europe."
"pool.ntp.org,        north-america.pool.ntp.org,        oceania.pool.ntp.org"
msgstr ""

#: dtime.cpp:274
#, kde-format
msgid "Unable to contact time server: %1."
msgstr ""

#: dtime.cpp:278
#, kde-format
msgid "Can not set date."
msgstr "Methu gosod dyddiad newydd."

#: dtime.cpp:281
#, fuzzy, kde-format
msgid "Error setting new time zone."
msgstr "Gwall wrth osod Amserfa newydd."

#: dtime.cpp:281
#, fuzzy, kde-format
#| msgid "Timezone Error"
msgid "Time zone Error"
msgstr "Gwall Cylchfa Amser"

#: dtime.cpp:299
#, fuzzy, kde-format
#| msgid ""
#| "<h1>Date & Time</h1> This control module can be used to set the system "
#| "date and time. As these settings do not only affect you as a user, but "
#| "rather the whole system, you can only change these settings when you "
#| "start the Control Center as root. If you do not have the root password, "
#| "but feel the system time should be corrected, please contact your system "
#| "administrator."
msgid ""
"<h1>Date & Time</h1> This system settings module can be used to set the "
"system date and time. As these settings do not only affect you as a user, "
"but rather the whole system, you can only change these settings when you "
"start the System Settings as root. If you do not have the root password, but "
"feel the system time should be corrected, please contact your system "
"administrator."
msgstr ""
"<h1>Dyddiad ac Amser</h1> Gellir y modiwl rheolaeth yma cael ei ddefnyddio i "
"osod dyddiad ac amser y cysawd.  Gan fod y gosodiadau yma yn effeithio dim "
"yn unig arnoch chi fel defnyddiwr, ond ar y cysawd cyfan, cewch newid y "
"gosodiadau yma dim ond wrth gychwyn y Ganolfan Reoli fel gwraidd.  Os nid "
"oes gennych y cyfrinair gwraidd, ond rydych yn teimlo dylai'r amser cysawd "
"cael ei cywiro, cysylltwch â gweinyddwr eich cysawd."

#: main.cpp:43
#, kde-format
msgid "KDE Clock Control Module"
msgstr "Modiwl Rheoli'r Cloc KDE"

#: main.cpp:47
#, kde-format
msgid "(c) 1996 - 2001 Luca Montecchiani"
msgstr ""

#: main.cpp:49
#, kde-format
msgid "Luca Montecchiani"
msgstr ""

#: main.cpp:49
#, kde-format
msgid "Original author"
msgstr "Awdur gwreiddiol"

#: main.cpp:50
#, kde-format
msgid "Paul Campbell"
msgstr ""

#: main.cpp:50
#, kde-format
msgid "Current Maintainer"
msgstr "Cynhaliwr Cyfredol"

#: main.cpp:51
#, kde-format
msgid "Benjamin Meyer"
msgstr ""

#: main.cpp:51
#, kde-format
msgid "Added NTP support"
msgstr ""

#: main.cpp:54
#, fuzzy, kde-format
#| msgid ""
#| "<h1>Date & Time</h1> This control module can be used to set the system "
#| "date and time. As these settings do not only affect you as a user, but "
#| "rather the whole system, you can only change these settings when you "
#| "start the Control Center as root. If you do not have the root password, "
#| "but feel the system time should be corrected, please contact your system "
#| "administrator."
msgid ""
"<h1>Date & Time</h1> This control module can be used to set the system date "
"and time. As these settings do not only affect you as a user, but rather the "
"whole system, you can only change these settings when you start the System "
"Settings as root. If you do not have the root password, but feel the system "
"time should be corrected, please contact your system administrator."
msgstr ""
"<h1>Dyddiad ac Amser</h1> Gellir y modiwl rheolaeth yma cael ei ddefnyddio i "
"osod dyddiad ac amser y cysawd.  Gan fod y gosodiadau yma yn effeithio dim "
"yn unig arnoch chi fel defnyddiwr, ond ar y cysawd cyfan, cewch newid y "
"gosodiadau yma dim ond wrth gychwyn y Ganolfan Reoli fel gwraidd.  Os nid "
"oes gennych y cyfrinair gwraidd, ond rydych yn teimlo dylai'r amser cysawd "
"cael ei cywiro, cysylltwch â gweinyddwr eich cysawd."

#: main.cpp:107
#, kde-format
msgid "Unable to authenticate/execute the action: %1, %2"
msgstr ""

#: main.cpp:127
#, kde-format
msgid "Unable to change NTP settings"
msgstr ""

#: main.cpp:138
#, kde-format
msgid "Unable to set current time"
msgstr ""

#: main.cpp:148
#, kde-format
msgid "Unable to set timezone"
msgstr ""

#~ msgid "kcmclock"
#~ msgstr "kcmclock"

#, fuzzy
#~| msgid "Timezone Error"
#~ msgid "Date/Time Error"
#~ msgstr "Gwall Cylchfa Amser"
